class Empleado: #molde que tiene varias cosas dentro, empaqueta la idea, plantilla
    def __init__(self, nombre, salario, tasa, antiguedad): #init es la funcion que inicializa los atributos, se ejcuta nada más crear al empleado 
        self.__nombre = nombre #self son atributos
        self.__salario = salario #los dos __ hago las cosas de forma privada
        self.__tasa = tasa
        self.__antiguedad= antiguedad
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre,
tax=self.__impuestos))
        return self.__impuestos
    def Antiguedad_emple (self): 
        print ("Este empleado: " + self.__nombre+ "lleva trabajando en esta empresa " + str (self.__antiguedad))
        if self.__antiguedad>1:
            print ("Con este empleado" + self.__nombre + "la empresa se ahorra un 10%") 

def displayCost(total):
     print("Los impuestos a pagar en total son {:.2f} euros".format(total))


emp1 = Empleado("Pepe", 20000, 0.35, 2)
emp2 = Empleado("Ana", 30000, 0.30, 1)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 4), Empleado("Luisa", 25000, 0.15, 14)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()


displayCost(total)
#la primera idea es utilizar clase 
#init: constructor, es elq inicia, es un método
#self a quien me están apuntando.