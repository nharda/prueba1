from rectangulo import Rectangulo #si yo   quiero llamar a una clase en concreto usamos from fichero import la clase
import unittest

class TestRectanguloTestCase(unittest.TestCase): #tenemos que poner en la definificion la misma palabra al principio que la clase
    def test_rectanguloTestPerimetro(self):
        rectangulo01= Rectangulo (1,2)
        perimetro1= rectangulo01.perimetro ()
        self.assertEqual(perimetro1, 25)

unittest.main()
