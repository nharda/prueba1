class Fraccion:
    def __init__ (self,num=0.0,den=1.0): #fraccion, algo que tiene numerador y denominador, los valores por defecto nos ahorra poner cosas que son lógicas.
        self.numerador=num
        self.denominador=den
    def sumar (izq, dcha):
        return Fraccion (izq.numerador* dcha.denominador + dcha.numerador*izq.denominador)
    def suma (self, otro):
        self.numerador= self.numerador * otro.denominador + self.denominador* otro.numerador   
        self.denominador= self.numerador * otro.denominador  
        return self 
    def resta (self, otro):
       self.numerador= self.numerador * otro.denominador - self.denominador* otro.numerador 
       self.denominador= self.numerador * otro.denominador  
       return self 
    def multiplicacion (self, otro):
        self.numerador= self.numerador* otro.numerador
        self.denominador= self.denominador*otro.denominador
        return self
    def division (self, otro):
        self.numerador= self.numerador*otro.denominador
        self.denominador= self.denominador*otro.numerador
        return self
    
    # def __add__ (self, )

fraccion1= Fraccion (1/2, 7/6)
fraccion2= Fraccion (2/3,8/9)

#si creamos una funcion no es una orden por lo que ponemos sumar , un método es una orden por lo tanto suma
unmedio= Fraccion (1,2)
uno = Fraccion (1)
cero= Fraccion ()

misuma= uno.suma (unmedio) #me lo suma a uno
misuma= Fraccion.sumar(uno, unmedio)
print (misuma)
